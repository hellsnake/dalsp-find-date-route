local pathres = "resources/src/lua/table/secondary/"
local role = require(pathres .. "Role")
local item = require(pathres .. "Item")
local string_en = require(pathres .. "en/String")
local pathdump = "routes_dump/"
local hero = require(pathres .. "Hero")


function pairsByKeys (t, f)
  local a = {}
  for n in pairs(t) do table.insert(a, n) end
  table.sort(a, f)
  local i = 0      -- iterator variable
  local iter = function ()   -- iterator function
    i = i + 1
    if a[i] == nil then return nil
    else return a[i], t[a[i]]
    end
  end
  return iter
end

function write_readme()
for k,v in pairsByKeys(role) do
  if v.nameId == 0 then goto continue end
  local name = string_en[v.nameId].text
  local likefood,likegift
  for kk,vv in pairsByKeys(v.likeFood) do
    likefood = string_en[item[kk].nameTextId].text
    break
  end
  for kk,vv in pairsByKeys(v.likeGift) do
    likegift = string_en[item[kk].nameTextId].text
    break
  end
  str = string.format("|%-5s|%-20s|%-25s|%-20s|\n",k,name,likegift,likefood)
  file:write(str)
  ::continue::
end
end

file = io.open(pathdump.."Favorite_Gifts.md","w+")
file:write("# Hobby for all Spirits in Date - City".."\n")
file:write("|ID|Spirit Name|Favorite Gift|Favorite Food|".."\n")
file:write("|--|--|--|--|".."\n")
write_readme()
file:close()