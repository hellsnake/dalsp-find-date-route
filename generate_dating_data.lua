-- Process dating_process
path = "resources/src/lua/table/secondary/"
pokedex_page = require(path .. "NewPokedexPage")
dating_rule = require(path .. "DatingRule")
dating_process = require(path .. "DatingProcess")
string_en = require(path .. "en/String")

function pairsByKeys (t, f)
  local a = {}
  for n in pairs(t) do table.insert(a, n) end
  table.sort(a, f)
  local i = 0      -- iterator variable
  local iter = function ()   -- iterator function
    i = i + 1
    if a[i] == nil then return nil
    else return a[i], t[a[i]]
    end
  end
  return iter
end

dating_data = {}
for script_id,tab in pairsByKeys(dating_process) do
  local tablename = dating_rule[script_id].callTableName
  local startingpoint = dating_rule[script_id].start_node_id
  dating_data[tablename] = dating_data[tablename] or {}
  dating_data[tablename][script_id] = dating_data[tablename][script_id] or {}
  dating_data[tablename][script_id]["start_node_id"] = startingpoint
  dating_data[tablename][script_id]["datingTitle"] = string_en[tab["titleName"]].text
  for i=1,5 do
    for k,v in ipairs(tab["end"..i]) do
      dating_data[tablename][script_id][v] = {}
      dating_data[tablename][script_id][v]["endType"] = i
      dating_data[tablename][script_id][v]["endSynopsis"] = string_en[tab["end"..i.."Synopsis"][k]].text
    end
  end
end

file = io.open("luagen/dating_data.lua","w+")
file:write("return {\n")
dent = "  "
for tablename,endlist in pairsByKeys(dating_data) do
--file = file:open("luagen/dating_data.lua","w+")
  file:write(dent.."[\""..tablename.."\"] = {\n")
  dent = dent.."  "
  for script_id,tab in pairsByKeys(endlist) do
    file:write(dent.."["..script_id.."] = {\n")
    dent = dent.."  "
    file:write(dent.."datingTitle = \""..tab["datingTitle"].."\",\n")
    file:write(dent.."start_node_id = "..tab["start_node_id"]..",\n")
    tab["start_node_id"]=nil
    tab["datingTitle"]=nil
    for ending_id,v in pairsByKeys(tab) do
      file:write(dent.."["..ending_id.."] = {\n")  
      dent = dent.."  "
      file:write(dent.."endType = "..v.endType..",\n")  
      file:write(dent.."endSynopsis = \""..v.endSynopsis.."\",\n")  
      dent = string.sub(dent,0,-3)
      file:write(dent.."},\n")
    end
    dent = string.sub(dent,0,-3)
    file:write(dent.."},\n")
  end
  dent = string.sub(dent,0,-3)
  file:write(dent.."},\n")
end
file:write("}")
file:close()