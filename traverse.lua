-- config, case insensitive
local loading_character = "Mayuri"

-- Some dicts for easy access
local loading_dict = {
  ["Tohka"] = "Dating101",
  ["Origami"] = "Dating102",
  ["Yoshino"] = "Dating103",
  ["Kurumi"] = "Dating104",
  ["Kotori"] = "Dating105",
  ["Kaguya"] = "Dating107",
  ["Yuzuru"] = "Dating108",
  ["Miku"] = "Dating110",
  ["Mayuri"] = "Dating112",
  ["Natsumi"] = "Dating113",
}
local ending_cat = {
  [1] = "HAPPY END",
  [2] = "TRUE END",
  [3] = "NORMAL END",
  [4] = "BAD END",
  [5] = "HIDDEN END"
}

-- load res
function firstToUpper(str) return (str:gsub("^%l", string.upper)) end
local loading_table = loading_dict[firstToUpper(string.lower(loading_character))]

local pathres = "resources/src/lua/table/secondary/"
local pathgen = "luagen/"
local date = require(pathres .. "en/" .. loading_table)
local dating_data = require(pathgen .. "dating_data")

local all_route = {}
local buff = {}
local ind=0
local function diag_text(id)
  return date[id].text
end

local function shallowcopy(orig)
  local orig_type = type(orig)
  local copy
  if orig_type == 'table' then
    copy = {}
    for orig_key, orig_value in pairs(orig) do
      copy[orig_key] = orig_value
    end
  else -- number, string, boolean, etc
    copy = orig
  end
  return copy
end

local function remove_by_find_till_end(tab,key)
  local buff = {}
  for k,v in ipairs(tab) do
    if v ~= key then
      table.insert(buff,tab[k])
    else
      return buff
    end
  end
end

local function traverse(id,script_id)
  local idlist = date[id].jump
  if idlist[1] ~= nil then
    for k, id_new in ipairs(idlist) do
      if table.getn(idlist) > 1 then
        table.insert(buff,id_new)
      end
      traverse(id_new,script_id)
      if table.getn(idlist) > 1 then
        buff = remove_by_find_till_end(buff,id_new)
      end
    end
  else
    if dating_data[loading_table][script_id][id] == nil then
      return
    end
    local end_Type = dating_data[loading_table][script_id][id].endType
    local endtext = dating_data[loading_table][script_id][id].endSynopsis
    all_route[script_id][end_Type] = all_route[script_id][end_Type] or {}
    all_route[script_id][end_Type][id] = all_route[script_id][end_Type][id] or {}
    all_route[script_id][end_Type][id]["text"] = all_route[script_id][end_Type][id]["text"] or endtext
    table.insert(all_route[script_id][end_Type][id],shallowcopy(buff))
  end
end

local function generate_all_routes()
  for script_id, v in pairs(dating_data[loading_table]) do
    local startpoint = v.start_node_id
    local willwrite = "\n\nAll Routes for scripts " .. script_id
    all_route[script_id] = all_route[script_id] or {}
    traverse(startpoint,script_id)
  end
end

local function getRoute(dating_title,endIndex)
  local str = ""
  local function getsi(dating_title)
    for k,v in pairs(dating_data[loading_table]) do
      if string.match(string.lower(v.datingTitle),string.lower(dating_title)) ~= nil then
        return k
      end
    end
  end
  local script_id = getsi(dating_title)
  if all_route[script_id][endIndex] ~= nil then
    for k,v in pairs(all_route[script_id][endIndex]) do
      str = str..(string.format("%-15s%s",ending_cat[endIndex],v.text).."\n")
      local current_pos = 0
      local current_len = 99
      for k2,v2 in ipairs(v) do
        if current_len > table.getn(v2) then
          current_pos = k2
          current_len = table.getn(v2)
        end
      end
      for k3,v3 in ipairs(v[current_pos]) do str = str..(date[v3].text.."\n") end
      str = str..("\n")
    end
  end
  return str
end

generate_all_routes()

for k,v in pairs(dating_data[loading_table]) do 
  local char = firstToUpper(string.lower(loading_character))
  local actual_datingTitle = v.datingTitle
  local str = ""
  local dating_title = string.gsub(string.gsub(actual_datingTitle,"%(","."),"%)",".")
  for i=1,5 do
    str = str..getRoute(dating_title,i)
  end
  file = io.open("routes_dump/Daily/"..char.." "..actual_datingTitle..".txt","w+")
  file:write(str)
  file:close()
end
