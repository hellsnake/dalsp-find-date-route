--[[ Logic: 
DatingRule hold script id and call table name for script dates
DatingProcess hold ending type, scriptid, and synopsis
String_en hold translated strings (synopsis)
Dating???_en hold translated script dates.
--]]
--[[Ending id:
end1 = Happy end
end2 = True end
end3 = Normal end
end4 = Bad end
end5 = Hidden end
Note: end?Synopsis call for the String_en TL'ed synopsis; end? call for ending scripts id
--]]
-- Kotori - Dating105_en -- 6: bakery 7: doll shop 12: School (night)
-- Kurumi - Dating104_en
function pairsByKeys (t, f)
  local a = {}
  for n in pairs(t) do table.insert(a, n) end
  table.sort(a, f)
  local i = 0      -- iterator variable
  local iter = function ()   -- iterator function
    i = i + 1
    if a[i] == nil then return nil
    else return a[i], t[a[i]]
    end
  end
  return iter
end
---- Print all choices
--for id, tab in pairsByKeys(date) do
--  if math.floor(id/1000000) == 119 then
--    nextidlist = tab.jump
--    if nextidlist[1] ~= nil then
--      listsize = table.getn(nextidlist)
--      if listsize ~= 1 then
--        print("CHOICES")
--        for i,id in pairs(nextidlist) do
--          print_diag(id)
--        end
--        print()
--      end
--    end
--  end
--end

ending_cat = {
  [1] = "HAPPY END",
  [2] = "TRUE END",
  [3] = "NORMAL END",
  [4] = "BAD END",
  [5] = "HIDDEN END"
}
end_type = "Normal"
function f(pattern)
for k,v in ipairs(ending_cat) do
  if string.find(string.lower(v),string.lower(pattern))~=nil then
    return k
  end
end
end
x=f(end_type)
print(x)
    
print(string.match(string.lower("HAPPY END"),string.lower("happy end")))