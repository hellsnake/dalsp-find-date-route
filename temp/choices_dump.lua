loading_table = "Dating113"

-- load res
path = "resources/src/lua/table/secondary/"
date = require(path .. "en/" .. loading_table)
dating_rule = require(path .. "DatingRule")

function pairsByKeys (t, f)
  local a = {}
  for n in pairs(t) do table.insert(a, n) end
  table.sort(a, f)
  local i = 0      -- iterator variable
  local iter = function ()   -- iterator function
    i = i + 1
    if a[i] == nil then return nil
    else return a[i], t[a[i]]
    end
  end
  return iter
end

function print_diag(id)
  print(date[id].text)
end

function print_choices(script_id)
  for k,v in pairsByKeys(date) do
    jumptab = v.jump
    if v.scriptId == script_id and table.getn(jumptab) > 1 then
      print("CHOICES")
      for k1,v1 in pairsByKeys(jumptab) do print_diag(v1) end
      print()
    end
  end
end


-- STARTS HERE
for k, v in pairsByKeys(dating_rule) do
  if v.callTableName == loading_table then
    print("Choices for scripts " .. k)
    print_choices(k)
    print()
  end
end