loading_table = "Dating113"

-- load res
path = "resources/src/lua/table/secondary/"
date = require(path .. "en/" .. loading_table)
dating_rule = require(path .. "DatingRule")
dating_process = require(path .. "DatingProcess")
string_en = require(path .. "en/String")
file = io.open("routes_dump/dumps/"..loading_table.."_routes.txt", "w+")

dent = ""
ending_cat = {
  [1] = "HAPPY END",
  [2] = "TRUE END",
  [3] = "NORMAL END",
  [4] = "BAD END",
  [5] = "HIDDEN END"
}
function diag_text(id)
  return date[id].text
end

function traverse(dent, id)
  end_Type = date[id].endType
  if end_Type ~= 0 then
    local scriptId = date[id].scriptId
    local endtable = dating_process[scriptId]["end" .. tostring(end_Type)]
    for k, v in pairs(endtable) do
      if v == id then
        local string_id = dating_process[scriptId]["end" .. tostring(end_Type) .. "Synopsis"][k]
        endtext = string_en[string_id].text
      end
    end
    local willwrite = "  " .. ending_cat[end_Type] .. " (" .. endtext .. ")"
    file:write(willwrite)
  end
  idlist = date[id].jump
  if idlist[1] ~= nil then
    if table.getn(idlist) ~= 1 then
      dent = dent .. "    "
    end
    for k, id_new in pairs(idlist) do
      if table.getn(idlist) ~= 1 then
        local willwrite = "\n" .. dent .. diag_text(id_new)
        file:write(willwrite)
      end
      traverse(dent, id_new)
    end
  else
    dent = string.sub(dent, 0, -3)
  end
end

function find_startingpoint(date)
  local tab = {}
  for k, v in pairs(dating_rule) do
    if v.callTableName == loading_table then
      tab[k] = v.start_node_id
    end
  end
  return tab
end

-- STARTS HERE
scripts_id = find_startingpoint(date)
for k, v in pairs(scripts_id) do
  local willwrite = "\n\nAll Routes for scripts " .. k
  file:write(willwrite)
  traverse(dent, v)
end
file:close()